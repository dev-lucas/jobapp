//
//  JobTableViewCell.swift
//  job-listings
//
//  Created by Lucas Gomes on 17/10/22.
//

import UIKit

class JobTableViewCell: UITableViewCell {

    @IBOutlet weak var locationLabel: UILabel?
    @IBOutlet weak var contractLabel: UILabel?
    @IBOutlet weak var postedAtLabel: UILabel?
    @IBOutlet weak var jobInfoOneBackgroundView: UIView?
    @IBOutlet weak var jobInfoTwoBackgroundView: UIView?
    @IBOutlet weak var logoImageView: UIImageView?
    @IBOutlet weak var backgroundViewCell: UIView?
    @IBOutlet weak var backgroundDetailViewCell: UIView?
    @IBOutlet weak var jobPositionLabel: UILabel?
    @IBOutlet weak var companyNameLabel: UILabel?
    @IBOutlet weak var languagesView: UIView?
    @IBOutlet weak var paddingView: UIView?
    
    func settingCell(job: JobModel) {
        guard let background = backgroundViewCell,
              let backgroundDetail = backgroundDetailViewCell,
              let logo = logoImageView,
              let companyName = companyNameLabel,
              let jobInfoOneBackground = jobInfoOneBackgroundView,
              let jobInfoTwoBackground = jobInfoTwoBackgroundView,
              let jobPosition = jobPositionLabel,
              let location = locationLabel,
              let contract = contractLabel,
              let postedAt = postedAtLabel,
              let languages = languagesView,
              let padding = paddingView
        else {
            return
        }

        let paddingWidth: CGFloat = 15
        let paddingHeight: CGFloat = 10
        var lastWidth: CGFloat = 0
        var lastHeight: CGFloat = 0
        var languagesViewHeight: CGFloat = 0
        var jobLanguages = job.languages
        
        padding.backgroundColor = ColorsUtil.white
        jobLanguages.insert(job.role, at: 0)
        jobLanguages.insert(job.level, at: 1)
        jobLanguages += job.tools
        languages.subviews.forEach({ $0.removeFromSuperview() })
        
        for language in jobLanguages {
            let view1 = UILabel()
            view1.backgroundColor = ColorsUtil.neutralFilterTablets
            view1.textAlignment = .center
            view1.textColor = ColorsUtil.primary
            view1.text = language
            view1.font = UIFont.boldSystemFont(ofSize: 16)
            view1.layer.cornerRadius = 5
            view1.layer.masksToBounds = true
            
            if lastWidth < languages.frame.width {
                view1.sizeToFit()
                view1.frame = CGRect(x: lastWidth, y: lastHeight, width: (view1.frame.width + paddingWidth), height: (view1.frame.height + paddingHeight))
                lastWidth += view1.frame.width + 10
            }
            
            if lastWidth > languages.frame.width {
                view1.sizeToFit()
                lastWidth = 0
                lastHeight += view1.frame.height + paddingHeight + 10
                view1.frame = CGRect(x: lastWidth, y: lastHeight, width: (view1.frame.width + paddingWidth), height: (view1.frame.height + paddingHeight))
                lastWidth += view1.frame.width + 10
            }

            languages.addSubview(view1)
            languagesViewHeight = view1.frame.origin.y + view1.frame.height
            print(job.company)
        }
        
        func updateLanguagesViewHeight(height: CGFloat) {
            if languages.frame.height != height {
                languages.removeConstraints(languages.constraints)
                let verticalConstraint = languages.heightAnchor.constraint(equalToConstant: height)
                NSLayoutConstraint.activate([verticalConstraint])
                print("Frame height: \(languages.frame.height)")
                print("Real height: \(height)")
                print()
                print()
            }
        }
        
        updateLanguagesViewHeight(height: paddingHeight + CGFloat(DecimalFormatterUtil.simple(n: languagesViewHeight)))
        logo.image = UIImage(named: job.logo)
        companyName.text = job.company
        jobPosition.text = job.position
        location.text = job.location
        contract.text = job.contract
        postedAt.text = job.postedAt
        
        background.addShadow()
        background.layer.cornerRadius = 5
        background.backgroundColor = ColorsUtil.white
        logo.layer.cornerRadius = 26
        backgroundDetail.backgroundColor = ColorsUtil.white
        jobPosition.textColor = ColorsUtil.neutralDarkGray
        
        jobInfoOneBackground.isHidden = true
        jobInfoTwoBackground.isHidden = true
        
        if job.new {
            jobInfoOneBackground.layer.cornerRadius = 12
            jobInfoOneBackground.backgroundColor = ColorsUtil.primary
            jobInfoOneBackground.isHidden = false
        }
 
        if job.featured {
            backgroundDetail.backgroundColor = ColorsUtil.primaryLight
            jobInfoTwoBackground.layer.cornerRadius = 12
            jobInfoTwoBackground.backgroundColor = ColorsUtil.neutralDarkGray
            jobInfoTwoBackground.isHidden = false
        }
        
        backgroundDetail.layer.cornerRadius = 5
        backgroundDetail.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
}
