//
//  ViewController.swift
//  job-listings
//
//  Created by Lucas Gomes on 16/10/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var jobsTableView: UITableView!
    @IBOutlet var mainView: UIView!
    var jobs: [JobModel]? = DecodableUtil.jobs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTableView()
    }
    
    func settingTableView() {
        jobsTableView.register(UINib(nibName: "JobTableViewCell", bundle: nil), forCellReuseIdentifier: "JobTableViewCell")
        jobsTableView.dataSource = self
        jobsTableView.delegate = self
        jobsTableView.sectionHeaderTopPadding = 0
        jobsTableView.sectionFooterHeight = 0
        jobsTableView.backgroundColor = ColorsUtil.primary
        mainView.backgroundColor = ColorsUtil.primary
        
        //Change only bottom color on tableView
        jobsTableView.tableFooterView = UIView(frame: CGRect.zero)
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 1000))
        footerView.backgroundColor = ColorsUtil.neutralBackground
        jobsTableView.tableFooterView?.addSubview(footerView)
    }
    
    private func getHeaderHeightByInterface() -> CGFloat {
        switch UIDevice.current.userInterfaceIdiom {
        case UIUserInterfaceIdiom.phone: return 190
        case UIUserInterfaceIdiom.pad: return 320
        default: return 320
        }
    }
}

extension ViewController: UITableViewDataSource {
 
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let jobs = jobs else { return UITableViewCell() }
        guard let cell = jobsTableView.dequeueReusableCell(withIdentifier: "JobTableViewCell") as? JobTableViewCell else { fatalError("Generate cell error") }
        //print(jobs[indexPath.row])
        
        cell.settingCell(job: jobs[indexPath.row])
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HeaderView", owner: self)?.first as? HeaderView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return getHeaderHeightByInterface()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}



