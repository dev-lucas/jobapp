//
//  ColorsUtil.swift
//  job-listings
//
//  Created by Lucas Gomes on 17/10/22.
//

import UIKit

class ColorsUtil: NSObject {
    static public let white = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    static public let primary = UIColor(red: 91.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1)
    static public let primaryLight = UIColor(red: 100.0/255.0, green: 186.0/255.0, blue: 187.0/255.0, alpha: 1)
    static public let neutralBackground = UIColor(red: 239.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1)
    static public let neutralFilterTablets = UIColor(red: 238.0/255.0, green: 246.0/255.0, blue: 246.0/255.0, alpha: 1)
    static public let neutralGray = UIColor(red: 123.0/255.0, green: 142.0/255.0, blue: 142.0/255.0, alpha: 1)
    static public let neutralDarkGray = UIColor(red: 44.0/255.0, green: 58.0/255.0, blue: 58.0/255.0, alpha: 1)
}
