//
//  DecimalFormatterUtil.swift
//  job-listings
//
//  Created by Lucas Gomes on 20/10/22.
//

import UIKit

class DecimalFormatterUtil: NSObject {
    
    static func simple(n: CGFloat) -> Int {
        return Int(String(format: "%.0f", n)) ?? 0
    }
 
    
}
