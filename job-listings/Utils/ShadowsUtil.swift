//
//  ShadowsUtil.swift
//  job-listings
//
//  Created by Lucas Gomes on 18/10/22.
//

import UIKit

extension UIView {
    func addShadow() {
        layer.shadowColor = ColorsUtil.primary.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = .zero
        layer.shadowRadius = 7
        //layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
