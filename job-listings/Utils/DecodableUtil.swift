//
//  DecodableUtil.swift
//  job-listings
//
//  Created by Lucas Gomes on 16/10/22.
//

import Foundation

class DecodableUtil {
    static public var jobs: [JobModel]? = parseJSON()
    
    static private func parseJSON() -> [JobModel]? {
        guard let path = Bundle.main.path(forResource: "data", ofType: "json") else { return [] }
        let url = URL(fileURLWithPath: path)
        var result: [JobModel]?
        
        do {
            let jsonData = try Data(contentsOf: url)
            result = try JSONDecoder().decode([JobModel].self, from: jsonData)
            return result
        } catch {
            print("Error: \(error.localizedDescription)")
            return []
        }
    }
}
