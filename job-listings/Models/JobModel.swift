//
//  JobModel.swift
//  job-listings
//
//  Created by Lucas Gomes on 17/10/22.
//

import UIKit

struct JobModel: Codable {
    let id: Int
    let company: String
    let logo:  String
    let new:  Bool
    let featured:  Bool
    let position:  String
    let role:  String
    let level:  String
    let postedAt:  String
    let contract:  String
    let location:  String
    let languages:  [String]
    let tools:  [String]
}
